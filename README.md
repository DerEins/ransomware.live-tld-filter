# Agrégateur et filtre par domaine de premier niveau de victimes recensée sur Ransomware.live

👤 **[Mathieu Dupoux](mailto:contact@mathieudupoux.fr)**

> Dans le cadre d'un cours de [Renseignement d'Origine Source Ouverte (ROSO)](https://fr.wikipedia.org/wiki/Renseignement_d%27origine_sources_ouvertes) ou OSINT, il nous a été demandé d'utiliser la base de donnée de [Ransomware.live](https://www.ransomware.live) afin de retrouver les victimes de ransomware à nom de domaine de premier niveau (Top-Level Domain ou TLD) en `.fr`.

## Prise en main
Pour utiliser ce script, il suffit de cloner ce dépôt puis d'exécuter le script Python `getVictims.py` :
```sh
python3 getVictims.py -d <tld>
```
### Options
- `-d <tld>` (requis): Le domaine de premier niveau (Top-Level Domain) que l'on souhaite filtrer. Dans notre contexte, on entrera `.fr`.
- `-o <path>` (optionnel) : Un fichier dans lequel exporter le résultat en format JSON. S'il n'est pas précisé, le résultat du filtrage est uniquement donné en console.
 
## Méthodologie
Il existe deux méthodes sur Ransomware.live pour récupérer les données dans un format exploitable. La méthode la plus efficace aurait été d'utiliser [l'API du service](https://api.ransomware.live/apidocs/) afin d'effectuer une requête optimisée comportant uniquement les résultats souhaités. Malheureusement, celle-ci ne permet pas de demander une liste de victimes par nom de domaine ni une liste complète de victimes. Ceci n'est possible que pour les victimes récentes ou avec une recherche par mois-année. Ainsi, il est plus pratique de récupérer la liste complète des victimes qui est directement téléchargeable [au format JSON](https://data.ransomware.live/posts.json) (aussi disponible en [CSV](https://www.ransomware.live/posts.csv)), même si cela implique de récupérer beaucoup de données inutiles qui nécessite un filtrage a posteriori.

Le script `getVictims.py` développé pour l'occasion permet de télécharger la liste des victimes en JSON depuis le site Web de ransomware.live puis de la filtrer selon le domaine de premier niveau donné en paramètre (voir [Options](#options)), et non uniquement `.fr`. 

Afin d'éviter les faux-positif, nous choisirons de filtrer simplement les champs `website` dont les valeurs se terminent par `.fr` à l'aide de la . Cependant, une analyse rapide de la base de donnée nous montre deux problèmes :
- Les entrée les plus anciennes ne comportent pas d'entrée `website` alors que beaucoup d'entre elles ont le site web attaqué comme (`post_title`). Ainsi, ce champ est inclut au filtrage.
- Certaines entrées sont de la forme `mondomaine.fr/` et non `mondomaine.fr`, ce qui oblige à inclure aussi les champs se terminant par `.fr/`.
  
Cette méthode consistant à ne conserver que les champs se terminant par leur TLD est cependant limitée puisque certaines entrées pourraient contenir du texte ou un simple espace blanc après le TLD, les excluant ainsi du filtrage. On notera aussi que cette méthode est limitée pour recenser l'intégralité des victimes de ransomware françaises, celles-ci n'ayant pas nécessairement un nom de domaine en `.fr`

## Résultats

À la date du 14 octobre 2023, le script permet de recenser **84** victimes en `.fr` de ransomware depuis le 30 septembre 2021.
