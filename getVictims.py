import requests
import json
from datetime import datetime
import argparse

# Setup script arguments
parser = argparse.ArgumentParser(
    description="Fetch ransomware victims from Ransomware.live and filter them by top-level domain.")
parser.add_argument("-d", "--domain", metavar="TLD",
                    help="Top-level domain to filter", required=True)
parser.add_argument("-o", "--output", metavar="FILENAME",
                    help="Output JSON file for filtered data")


def get_data(url):
    """
    Get JSON data from ransomware.live

    Args :
        url (string) : rasomware.live JSON data

    Returns :
        Fetched data in JSON format
    """
    return requests.get(url).json()


def tld_filter(tld, item):
    """
    Top-level tld filter function.

    Args :
        tld (string) : Top-level domain to search.
        item : Item to filter.

    Returns :
        bool : True if tld is in item title or website.
    """

    return item["post_title"].endswith(tld[0]) or item["post_title"].endswith(tld[0]) or ("website" in item and (item["website"].endswith(tld[0]) or item["website"].endswith(tld[1])))


def print_result(res):
    """
    Print filtered data to console.

    Arg :
        res : Filtered ransomware.live data.
    """
    for item in res:
        published_date = datetime.strptime(
            item['published'], '%Y-%m-%d %X.%f').strftime('%c')
        print(
            f"{item['post_title']}\n\t Published Date : {published_date}\n\t Ransomware/Group : {item['group_name']}\n")


def export_json_result(res, path):
    """
    Export filtered data in JSON file.

    Args :
        res : Filtered ransomware.live data.
        path : File for result export.
    """
    with open(f'{path}', 'w') as f:
        f.write('[' + ',\n'.join(json.dumps(item, indent=4)
                for item in res) + ']\n')


if __name__ == "__main__":
    # Get arguments
    args = vars(parser.parse_args())
    # Get domain and setup URL
    tld = (f"{args['domain']}", f"{args['domain']}/")
    url = "https://data.ransomware.live/posts.json"

    # Fetch and filter data
    data = get_data(url)
    result = list(filter(lambda item: tld_filter(tld, item), data))
    nbVictims = len(result) if len(result) > 0 else "No"

    # Output data if needed or print it
    if nbVictims != "No":
        if args['output']:
            path = f"{args['output']}"
            export_json_result(result, path)
        else:
            print_result(result)

    print(f"{nbVictims} victims with tld {tld[0]} found !")
